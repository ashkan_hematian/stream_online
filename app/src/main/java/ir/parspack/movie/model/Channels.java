package ir.parspack.movie.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ashkan on 12/24/17.
 */

public class Channels implements Parcelable {
    long id;
    String title;
    @SerializedName("img")
    String imageUrl;
    @SerializedName("url")
    String link;
    @SerializedName("programs")
    List<Plans> plans;

    public long getId() {
        return id;
    }
    public String getTitle() {
        return title;
    }
    public String getImageUrl() {
        return imageUrl;
    }
    public String getLink() {
        return link;
    }
    public List<Plans> getPlans() {
        return plans;
    }


    @Override
    public int describeContents() {
        return 0;
    }
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.title);
        dest.writeString(this.imageUrl);
        dest.writeString(this.link);
        dest.writeTypedList(this.plans);
    }
    public Channels() {
    }
    protected Channels(Parcel in) {
        this.id = in.readLong();
        this.title = in.readString();
        this.imageUrl = in.readString();
        this.link = in.readString();
        this.plans = in.createTypedArrayList(Plans.CREATOR);
    }
    public static final Creator<Channels> CREATOR = new Creator<Channels>() {
        @Override
        public Channels createFromParcel(Parcel source) {
            return new Channels(source);
        }

        @Override
        public Channels[] newArray(int size) {
            return new Channels[size];
        }
    };
}
