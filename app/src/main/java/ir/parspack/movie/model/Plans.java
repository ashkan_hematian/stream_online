package ir.parspack.movie.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ashkan on 12/24/17.
 */

public class Plans implements Parcelable {

    String title;
    long start;
    long end;


    public String getTitle() {
        return title;
    }
    public long getStart() {
        return start;
    }
    public long getEnd() {
        return end;
    }


    @Override
    public int describeContents() {
        return 0;
    }
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeLong(this.start);
        dest.writeLong(this.end);
    }
    public Plans() {
    }
    protected Plans(Parcel in) {
        this.title = in.readString();
        this.start = in.readLong();
        this.end = in.readLong();
    }
    public static final Creator<Plans> CREATOR = new Creator<Plans>() {
        @Override
        public Plans createFromParcel(Parcel source) {
            return new Plans(source);
        }

        @Override
        public Plans[] newArray(int size) {
            return new Plans[size];
        }
    };
}
