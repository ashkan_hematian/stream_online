package ir.parspack.movie.model.viewModel;

import ir.parspack.movie.model.Channels;

/**
 * Created by ashkan on 12/24/17.
 */

public class ReceiveModel {

    Channels tv1;
    Channels tv2;

    public Channels getTv1() {
        return tv1;
    }
    public Channels getTv2() {
        return tv2;
    }
}
