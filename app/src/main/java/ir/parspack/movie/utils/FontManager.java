package ir.parspack.movie.utils;

import android.graphics.Typeface;

import ir.parspack.movie.base.MasterApplication;


public class FontManager {

  private static FontManager sInstance;
  private Typeface IranSans;
  private Typeface IranSansBold;
  private FontManager(){}

  public static FontManager getInstance(){
    if(sInstance==null)
      sInstance = new FontManager();
    return sInstance;
  }

  public Typeface getFont(int index){
    switch (index){
      default:
      case 1: return  getIranSans();
      case 2: return getIranSansBold();
    }
  }

  public Typeface getIranSans() {
    if(sInstance.IranSans==null)
      sInstance.IranSans = Typeface.createFromAsset(MasterApplication.sInstance.getAssets(),"fonts/iran_sans_mobile.ttf");
    return sInstance.IranSans;
  }

  public Typeface getIranSansBold() {
    if(sInstance.IranSansBold==null)
      sInstance.IranSansBold = Typeface.createFromAsset(MasterApplication.sInstance.getAssets(),"fonts/iran_sans_mobile_bold.ttf");
    return sInstance.IranSansBold;
  }

  static public Typeface GetFontTypeFace (boolean isBold) {
    Typeface tf;
    if(isBold)
      tf = Typeface.createFromAsset(MasterApplication.sInstance.getAssets(), "iran_sans_mobile.ttf");
    else
      tf = Typeface.createFromAsset(MasterApplication.sInstance.getAssets(), "iran_sans_mobile_bold.ttf");

    return tf;
  }
}
