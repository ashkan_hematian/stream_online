package ir.parspack.movie.customView;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import ir.parspack.movie.R;
import ir.parspack.movie.utils.FontHelper;


/**
 * Created by Ashkan on 5/29/2017.
 */
public class CustomTextView extends AppCompatTextView {
    public CustomTextView(Context context) {
        super(context);
        init(null);
    }

    public CustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public CustomTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        if (!isInEditMode()) {
            if (attrs != null) {
                TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.CustomViewFont);
                int font = a.getInt(R.styleable.CustomViewFont_mcv_font, 1);
                setTypeface(FontHelper.getInstance().getFont(font));
                a.recycle();
            } else {
                setTypeface(FontHelper.getInstance().getIranSans());
            }
        }
    }


}
