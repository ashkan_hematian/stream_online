package ir.parspack.movie.base;

/**
 * Created by ashkan on 8/15/17.
 */

public class Constants {

    public final static String TAG_APP = "DivarNotes";

    public final static String TAG_FUNCTION_ERROR = TAG_APP + "_Function";
    public final static String TAG_DATABASE_ERROR = TAG_APP + "_Database";
    public final static String TAG_CONNECTION_ERROR = TAG_APP + "_Connection";



}
