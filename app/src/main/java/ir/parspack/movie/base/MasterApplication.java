package ir.parspack.movie.base;

import android.app.Application;
import android.graphics.Bitmap;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

import ir.parspack.movie.R;


/**
 * Created by Ashkan on 5/29/2017.
 */

public class MasterApplication extends Application {

    public static MasterApplication sInstance;

    @Override
    public void onCreate() {
        super.onCreate();


        sInstance = this;

        ImageLoader m = ImageLoader.getInstance();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
                .threadPoolSize(3)
                .diskCacheExtraOptions(480, 320, null)
                .defaultDisplayImageOptions(getDisplayImageOption())
                .build(); //discCache(new UnlimitedDiscCache(cacheDir))
        m.init(config);

    }

    public DisplayImageOptions getDisplayImageOption() {
        DisplayImageOptions options = new DisplayImageOptions
                .Builder()
                .showImageOnLoading(R.mipmap.img_default)
                .showImageForEmptyUri(R.mipmap.img_default)
                .showImageOnFail(R.mipmap.img_default)
                .cacheInMemory(false)
                .cacheOnDisk(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .displayer(new FadeInBitmapDisplayer(1000))
                .build();
        return options;
    }

}