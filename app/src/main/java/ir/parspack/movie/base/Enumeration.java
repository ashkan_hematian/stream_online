package ir.parspack.movie.base;


public class Enumeration {

    public enum ErrorItemType {

        StatusBadRequest400(0, "Bad Request", 400),
        StatusUnAuthorized401(1, "Unauthorized", 401),
        StatusForbidden403(2, "Forbidden", 403),
        StatusNotFound404(3, "Not Found", 404),
        StatusInternalServerError500(4, "Internal Server Error", 500);

        String name;
        int id;
        int statusCode;

        ErrorItemType(int id, String name, int statusCode) {
            this.id = id;
            this.name = name;
            this.statusCode = statusCode;
        }

        public int getId() {
            return id;
        }

        public int getStatusCode() {
            return statusCode;
        }

        public String getName() {
            return name;
        }

        public static ErrorItemType getById(int id) {
            switch (id) {
                default:
                case 0:
                    return StatusBadRequest400;
                case 1:
                    return StatusUnAuthorized401;
                case 2:
                    return StatusForbidden403;
                case 3:
                    return StatusNotFound404;
                case 4:
                    return StatusInternalServerError500;
            }
        }

    }


}
