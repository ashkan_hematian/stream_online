package ir.parspack.movie.connection;

import ir.parspack.movie.model.viewModel.ReceiveModel;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;

public interface APIServices {

    @Headers({"Content-Type:application/json-patch+json"})
    @GET("temp/daia/index.php?act=getPrograms")
    Call<ReceiveModel> ChanelList();

}