package ir.parspack.movie.connection;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ir.parspack.movie.R;
import ir.parspack.movie.base.MasterApplication;
import ir.parspack.movie.chainOfResponsibility.ChainOfResponsibility;
import ir.parspack.movie.chainOfResponsibility.ErrorFile;
import ir.parspack.movie.model.viewModel.ReceiveModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by ashkan on 8/16/17.
 */

public class ChannelListConnection {

    Context context = MasterApplication.sInstance;
    private ConnectionInterface mListener;

    public void changeUserStatus(ConnectionInterface listener) {
        this.mListener = listener;
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://hamed.afshar.ir/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        APIServices service = retrofit.create(APIServices.class);
        Call<ReceiveModel> call = service.ChanelList();

        call.enqueue(new Callback<ReceiveModel>() {
            @Override
            public void onResponse(Call<ReceiveModel> call, Response<ReceiveModel> response) {
                Log.i("onResponse", response.toString());
                int statusCode = 500;

                //test
                ErrorFile errorFile = new ErrorFile(statusCode, response.body());
                ChainOfResponsibility.callChainHandler(errorFile);


                if (response.isSuccessful()) {
                    mListener.successListener(response.body());
                } else {
//                    ErrorFile errorFile = new ErrorFile(statusCode, response.body());
//                    ChainOfResponsibility.callChainHandler(errorFile);

                    mListener.errorListener(context.getString(R.string.error_server_connection));
                }
            }

            @Override
            public void onFailure(Call<ReceiveModel> call, Throwable t) {
                Log.e("onFailure", t.getLocalizedMessage());
                mListener.errorListener(context.getString(R.string.error_received_data));
            }
        });
    }


}
