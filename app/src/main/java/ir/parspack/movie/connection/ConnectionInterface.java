package ir.parspack.movie.connection;

/**
 * Created by ashkan on 8/16/17.
 */

public interface ConnectionInterface {
    void errorListener(String txt);
    void successListener(Object object);
}

