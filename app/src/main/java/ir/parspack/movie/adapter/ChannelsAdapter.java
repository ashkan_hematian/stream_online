package ir.parspack.movie.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

import ir.parspack.movie.R;
import ir.parspack.movie.model.Channels;

public class ChannelsAdapter<T extends Channels> extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<T> data = new ArrayList<>();
    private View.OnClickListener mCallListener;

    public ChannelsAdapter(View.OnClickListener mCallListener) {
        this.mCallListener = mCallListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_dashboard_channel, parent, false);

        RecyclerView.ViewHolder vh;
        vh = new DefaultViewHolder(view);

        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        DefaultViewHolder mHolder = (DefaultViewHolder) holder;
        T node = data.get(position);

        mHolder.title_tv.setText(node.getTitle());
        ImageLoader.getInstance().displayImage(node.getImageUrl(), mHolder.logo_iv);

        mHolder.baseView.setTag(node);
        mHolder.baseView.setOnClickListener(mCallListener);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setData(List<T> data) {
        this.data = data;
    }

    //region viewHolder
    public class DefaultViewHolder extends RecyclerView.ViewHolder {

        protected View baseView;
        protected TextView title_tv;
        protected ImageView logo_iv;



        public DefaultViewHolder(View view) {
            super(view);
            baseView = view;

            title_tv = (TextView) view.findViewById(R.id.title_textView);
            logo_iv = (ImageView) view.findViewById(R.id.logo_imageView);

        }

    }
    //endregion
}
