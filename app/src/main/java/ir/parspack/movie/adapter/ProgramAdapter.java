package ir.parspack.movie.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ir.parspack.movie.R;
import ir.parspack.movie.model.Plans;

public class ProgramAdapter<T extends Plans> extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<T> data = new ArrayList<>();
    private View.OnClickListener mCallListener;

    public ProgramAdapter(View.OnClickListener mCallListener) {
        this.mCallListener = mCallListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_player_channel, parent, false);

        RecyclerView.ViewHolder vh;
        vh = new DefaultViewHolder(view);

        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        DefaultViewHolder mHolder = (DefaultViewHolder) holder;
        T node = data.get(position);

        mHolder.title_tv.setText(node.getTitle());

        mHolder.baseView.setTag(node);
        mHolder.baseView.setOnClickListener(mCallListener);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setData(List<T> data) {
        this.data = data;
    }

    //region viewHolder
    public class DefaultViewHolder extends RecyclerView.ViewHolder {

        protected View baseView;
        protected TextView title_tv;



        public DefaultViewHolder(View view) {
            super(view);
            baseView = view;

            title_tv = (TextView) view.findViewById(R.id.title_textView);

        }

    }
    //endregion
}
