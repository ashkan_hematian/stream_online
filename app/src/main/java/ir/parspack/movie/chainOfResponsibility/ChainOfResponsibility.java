package ir.parspack.movie.chainOfResponsibility;


/**
 * Created by ashkan on 12/26/17.
 */

public class ChainOfResponsibility {

    public interface Handler {
        public static String TAG = "Handler";
        public void setNextHandler(Handler handler);
        public void process(ErrorFile errorFile);
        public String getNextHandler();

    }

    public static void callChainHandler(ErrorFile errorFile) {
        BadRequestHandler firstHandler = new BadRequestHandler();
        firstHandler.process(errorFile);
    }

}
