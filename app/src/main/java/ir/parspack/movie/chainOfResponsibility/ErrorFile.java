package ir.parspack.movie.chainOfResponsibility;

/**
 * Created by ashkan on 12/26/17.
 */

public class ErrorFile {

    private final int httpStatus;
    private final Object file;

    public ErrorFile(int httpStatus, Object file){
        this.httpStatus = httpStatus;
        this.file = file;
    }


    public int getStatus() {
        return httpStatus;
    }

    public Object getFile() {
        return file;
    }

}
