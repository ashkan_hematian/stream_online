package ir.parspack.movie.chainOfResponsibility;

import android.util.Log;

import ir.parspack.movie.base.Enumeration;

/**
 * Created by ashkan on 12/26/17.
 */

public class BadRequestHandler implements ChainOfResponsibility.Handler {

    private ChainOfResponsibility.Handler handler;
    private String handlerName;

    public BadRequestHandler(){
        this.handlerName = BadRequestHandler.class.getSimpleName();
        setNextHandler(new ForbiddenHandler());
    }

    @Override
    public void setNextHandler(ChainOfResponsibility.Handler handler) {
        this.handler = handler;
    }

    @Override
    public void process(ErrorFile errorFile) {

        if(errorFile.getStatus() == Enumeration.ErrorItemType.StatusBadRequest400.getStatusCode()){
            Log.i(TAG, "Process " + Enumeration.ErrorItemType.StatusBadRequest400.getName() +" by "+ handlerName);
        }else if(handler!=null){
            Log.i(TAG, handlerName+" fowards error to " + handler.getNextHandler());
            handler.process(errorFile);
        }else{
            System.out.println("Error not supported");
        }

    }

    @Override
    public String getNextHandler() {
        return handlerName;
    }

}
