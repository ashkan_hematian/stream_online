package ir.parspack.movie.chainOfResponsibility;

import android.util.Log;

import ir.parspack.movie.base.Enumeration;

/**
 * Created by ashkan on 12/26/17.
 */

public class ForbiddenHandler implements ChainOfResponsibility.Handler {

    private ChainOfResponsibility.Handler handler;
    private String handlerName;

    public ForbiddenHandler(){
        this.handlerName = ForbiddenHandler.class.getSimpleName();
        setNextHandler(new InternalServerErrorHandler());
    }

    @Override
    public void setNextHandler(ChainOfResponsibility.Handler handler) {
        this.handler = handler;
    }

    @Override
    public void process(ErrorFile errorFile) {

        if(errorFile.getStatus() == Enumeration.ErrorItemType.StatusForbidden403.getStatusCode()){
            Log.i(TAG, "Process " + Enumeration.ErrorItemType.StatusForbidden403.getName() +" by "+ handlerName);
        }else if(handler!=null){
            Log.i(TAG, handlerName+" fowards error to " + handler.getNextHandler());
            handler.process(errorFile);
        }else{
            System.out.println("Error not supported");
        }

    }

    @Override
    public String getNextHandler() {
        return handlerName;
    }

}
